import express from "express";
import json from 'body-parser';
export const router = express.Router();

router.get('/', (req, res) => {
    const params = {
        num_recibo: req.query.num_recibo || '',
        nombre: req.query.nombre || '',
        domicilio: req.query.domicilio || '',
        tipo: req.query.tipo || '',
        kilowatts: req.query.kilowatts || ''
    }
    res.render('index', params);
});

router.post('/recibos', (req, res) => {

    let costo = 0;
    let tipo = req.body.tipo;
    
    if (tipo === "1") {
        costo = 1.08;
        tipo = "Domestico";
    }

    else if (tipo === "2") {
        costo = 2.5;
        tipo = "Comercial";
    }

    else if (tipo === "3") {
        costo = 3.0;
        tipo = "Industrial";
    }

    const params = {
        nombre: req.body.nombre || '',
        tipo: tipo,
        kilowatts: req.body.kilowatts || '',
        costo: costo
    }
    
    res.render('recibos', params);
});


export default { router };